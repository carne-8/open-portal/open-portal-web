module Lottie

open Feliz
open Fable.Core.JsInterop


let lottie: obj = importDefault "react-lottie"

type Options =
    { loop: bool
      autoplay: bool
      animationData: obj
      renderSettings:
        {| preserveAspectRatio: string |} }

type Lottie =

    static member inline width (value: int) = "width" ==> value
    static member inline height (value: int) = "height" ==> value
    static member inline options (value: Options) = "options" ==> value
    static member inline isClickToPauseDisabled (value: bool) = "isClickToPauseDisabled" ==> value

    static member inline create props = Interop.reactApi.createElement (lottie, createObj !!props)