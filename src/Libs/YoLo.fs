[<AutoOpen>]
module YoLo

open Fable.Core

[<Emit("btoa($0)")>]
let toBase64String (_string: string) : string = failwith "JS"

type Loadable<'Data> =
    | Loaded of 'Data
    | Loading of 'Data option


module Array =
    let allEven (list: _ []) =
        list
        |> Array.indexed
        |> Array.filter (fun (index, x) -> index % 2 = 0)
        |> Array.map (fun (index, x) -> x)

    let allOdd (list: _ []) =
        list
        |> Array.indexed
        |> Array.filter (fun (index, x) -> index % 2 = 1)
        |> Array.map (fun (index, x) -> x)


module Thoth =
    module Fetch =
        open Thoth.Fetch

        let errorToString error =
            match error with
            | DecodingFailed error -> error
            | NetworkError error
            | PreparingRequestFailed error -> error.Message
            | FetchFailed error -> sprintf "%s: %i" error.StatusText error.Status

module Browser =
    [<Emit("/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)")>]
    let isMobile: bool = jsNative