module App.View

open Feliz
open Feliz.Router
open Fable.Core
open Browser.Dom

open Components
open Components.MaterialDesign
open Drawer.Types

JsInterop.importAll "./App.sass"

let getPageTitle page =
    match page with
    | Page.DeviceControl _ -> I18n.pages.devices.title
    | Page.Scenes _ -> I18n.pages.scenes.title
    | Page.Login _
    | Page.Loading -> ""

[<ReactComponent>]
let View model dispatch =
    let (isScrolled, setScrolled) = React.useState false

    let appContainer = document.querySelector "#app"

    window.addEventListener ("scroll", (fun _event ->
        if window.scrollY > 80. && not isScrolled then
            setScrolled true
            appContainer.classList.add "header-title-shown"
        elif window.scrollY <= 80. && isScrolled then
            setScrolled false
            appContainer.classList.remove "header-title-shown"
    ))


    let drawerContent =
        [ { Name = I18n.app.drawer.views
            Items =
                [ DrawerElement.Label
                    { Name = I18n.pages.scenes.title
                      Icon = Assets.CssIcons.``isax-category-2``
                      Selected = (match model.Page with
                                    | Page.Scenes _ -> true
                                    | _ -> false)
                      OnPress = (fun _ -> Router.Route.Scenes |> Msg.ChangePage |> dispatch) }

                  DrawerElement.Label
                    { Name = I18n.pages.devices.title
                      Icon = Assets.CssIcons.``isax-category-2``
                      Selected = (match model.Page with
                                    | Page.DeviceControl _ -> true
                                    | _ -> false)
                      OnPress = (fun _ -> Router.Route.Devices |> Msg.ChangePage |> dispatch) } ] } ]

    let settingsMenuContent =
        [ Menu.MenuElement.Item
            {| Icon = Some Assets.CssIcons.``isax-logout``
               Name = I18n.app.settings.logout
               OnPress = (fun _ -> MenuMsg.Logout |> Msg.MenuMsg |> dispatch) |} ]


    let content =
        React.fragment [
            match model.Page with
            | Page.Loading _
            | Page.Login _ -> ()
            | _ ->
                Html.header [
                    prop.children [
                        Html.span [
                            prop.classes [
                                "icon"
                                Assets.CssIcons.``isax-menu-1``
                            ]
                            prop.onClick (fun _ -> Msg.OpenDrawer |> dispatch)
                            prop.onContextMenu (fun _ ->
                                if Browser.isMobile then
                                    Msg.OpenDrawer |> dispatch
                            )
                        ]

                        Html.h1 [
                            prop.classes [ "page-title"; if isScrolled then "shown" ]
                            prop.text (model.Page |> getPageTitle)
                        ]

                        SettingsButton.View
                            {| Pressed = model.SettingsMenuOpened.IsSome
                               OnPress = (fun buttonPos ->
                                    match model.SettingsMenuOpened with
                                    | None ->
                                        buttonPos
                                        |> Msg.OpenSettingsMenu
                                        |> dispatch
                                    | Some _ -> Msg.CloseSettingsMenu |> dispatch
                               ) |}
                    ]
                ]

                Drawer.View
                    {| IsOpened = model.DrawerOpened
                       OnClose = (fun _ -> Msg.CloseDrawer |> dispatch)
                       Elements = drawerContent |}


            match model.SettingsMenuOpened with
            | Some (x, y) ->
                Menu.View
                    {| OriginPosition = x, y
                       OnClose = (fun _ -> Msg.CloseSettingsMenu |> dispatch)
                       Content = settingsMenuContent |}
            | None -> ()



            match model.Page with
            | Page.Login model ->
                Pages.Login.View.View
                    model
                    (PageMessage.Login >> Msg.PageMessage >> dispatch)
            | Page.DeviceControl model ->
                Pages.DeviceControl.View.View
                    model
                    (PageMessage.DeviceControl >> Msg.PageMessage >> dispatch)
            | Page.Scenes model ->
                Pages.Scenes.View.View
                    model
                    (PageMessage.Scenes >> Msg.PageMessage >> dispatch)
            | Page.Loading -> Components.LoadingOverlay.View false

            if model.Loading then Components.LoadingOverlay.View true
        ]


    React.router [
        router.pathMode
        router.onUrlChanged (fun segments ->
            segments
            |> Router.parseUrl
            |> Msg.RouteChanged
            |> dispatch
        )
        router.children content
    ]