namespace App

open API.Domoticz

[<RequireQualifiedAccess>]
type PageMessage =
    | Login of Pages.Login.Msg
    | DeviceControl of Pages.DeviceControl.Msg
    | Scenes of Pages.Scenes.Msg

[<RequireQualifiedAccess>]
type Page =
    | Loading
    | Login of Pages.Login.Model
    | DeviceControl of Pages.DeviceControl.Model
    | Scenes of Pages.Scenes.Model


[<RequireQualifiedAccess>]
type MenuMsg =
    | Logout


[<RequireQualifiedAccess>]
type Msg =
    | RouteChanged of Router.Route

    | PageMessage of PageMessage
    | DevicesLoaded of Result<Device [], Thoth.Fetch.FetchError>
    | ScenesLoaded of Result<Scene [], Thoth.Fetch.FetchError>

    | ChangePage of Router.Route
    | OpenDrawer
    | CloseDrawer

    | OpenSettingsMenu of float * float
    | CloseSettingsMenu

    | MenuMsg of MenuMsg

type Model =
    { Page: Page
      Loading: bool
      ServerInfos: ServerInfos option
      ErrorMessage: string option
      SettingsMenuOpened: (float * float) option
      DrawerOpened: bool }