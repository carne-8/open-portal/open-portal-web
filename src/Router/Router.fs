module App.Router

open Feliz.Router
open Elmish

[<RequireQualifiedAccess>]
type Route =
    | Login
    | Devices
    | Scenes
    | Home

let parseUrl (segments: string list) =
    match segments with
    | [ "login" ] -> Route.Login
    | [ "devices" ] -> Route.Devices
    | [ "scenes" ] -> Route.Scenes

    | _ -> Route.Home

let getRoutePath route =
    match route with
    | Route.Login -> "/login"
    | Route.Devices -> "/devices"
    | Route.Scenes -> "/scenes"
    | Route.Home -> "/"

let navigateToRoute route =
    route |> getRoutePath |> Router.navigatePath

let navigateToRouteCmd route =
    Cmd.ofSub (fun _ ->
        // Dispatch navigate event to end of "js queue"
        // let some time to onUrlChanged effect to be registered
        Browser.Dom.window.setTimeout ((fun _ -> navigateToRoute route), 0)
        |> ignore
    )