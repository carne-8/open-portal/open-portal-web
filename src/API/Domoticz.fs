module API.Domoticz

open Fable.Core
open Thoth.Fetch
open Fetch.Types

[<AutoOpen>]
module Types =
    type ServerInfos =
        { ServerAddress: string
          Token: string }

    type Idx = string

    [<StringEnum>]
    [<RequireQualifiedAccess>]
    type Status =
        | [<CompiledName "OK">] Ok
        | [<CompiledName "ERROR">] Error

    type DefaultResponse =
        { status: Status
          message: string option }

    type ResponseWithResult<'Content> =
        { result: 'Content
          status: Status
          message: string option }

    [<StringEnum>]
    type DeviceType =
        | [<CompiledName "AC">] Blind
        | [<CompiledName "RFY">] ExtendedBlind
        | [<CompiledName "Switch">] Switch

    [<StringEnum>]
    type DeviceAction =
        | [<CompiledName "On">] On
        | [<CompiledName "Off">] Off
        | [<CompiledName "Stop">] Stop
        | [<CompiledName "Toggle">] Toggle

    [<StringEnum>]
    type SceneStatus =
        | [<CompiledName "On">] On
        | [<CompiledName "Off">] Off
        | [<CompiledName "Mixed">] Mixed

    type Device =
        { Name: string
          SubType: DeviceType
          idx: Idx }

    type Scene =
        { LastUpdate: System.DateTime
          Name: string
          Status: SceneStatus
          idx: Idx }


let createHeaders serverInfos =
    [ Authorization serverInfos.Token ]

module Device =
    let getDevices serverInfos =
        let url = sprintf "%s/json.htm?type=command&param=getlightswitches" serverInfos.ServerAddress

        Fetch.tryGet<_, ResponseWithResult<Device []>> (url, headers = createHeaders serverInfos)
        |> Promise.mapResult (fun response ->
            response.result
            |> Array.sortBy (fun device -> device.Name.ToLower())
        )


    let toggleDevice serverInfos (idx: Idx) (deviceAction: DeviceAction) =
        let url =
            sprintf "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=%A"
                serverInfos.ServerAddress
                idx
                deviceAction

        Fetch.tryGet<_, DefaultResponse> (url, headers = createHeaders serverInfos)


module Scene =
    let getScenes serverInfos =
        let url = sprintf "%s/json.htm?type=scenes" serverInfos.ServerAddress

        Fetch.tryGet<_, ResponseWithResult<Scene []>> (url, headers = createHeaders serverInfos)
        |> Promise.mapResult (fun x -> x.result)

    let launchScene serverInfos (idx: Idx) =
        let url =
            sprintf "%s/json.htm?type=command&param=switchscene&idx=%s&switchcmd=On"
                serverInfos.ServerAddress
                idx

        Fetch.tryGet<_, DefaultResponse> (url, headers = createHeaders serverInfos)