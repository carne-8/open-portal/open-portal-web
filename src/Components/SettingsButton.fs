module Components.SettingsButton

open Assets
open Feliz
open Fable.Core.JsInterop

importAll "./SettingsButton.sass"

[<ReactComponent>]
let View (props: {| Pressed: bool ; OnPress: (float * float) -> unit |}) =
    let elementRef = React.useElementRef ()

    Html.div [
        prop.ref elementRef

        prop.classes [
            "icon"
            "settings-button"

            if props.Pressed then "pressed"
        ]

        prop.onClick (fun _ ->
            let buttonRect = elementRef.current.Value.getBoundingClientRect()
            let x = buttonRect.left + buttonRect.width - 120.
            let y = buttonRect.top + buttonRect.height + 5.

            (x, y) |> props.OnPress
        )
        prop.onContextMenu (fun _ ->
            if Browser.isMobile then
                let buttonRect = elementRef.current.Value.getBoundingClientRect()
                let x = buttonRect.left + buttonRect.width - 120.
                let y = buttonRect.top + buttonRect.height + 5.

                (x, y) |> props.OnPress
        )

        prop.children [
            Html.span [
                prop.classes [
                    "linear-icon"
                    CssIcons.``isax-setting-24``
                ]
            ]
            Html.span [
                prop.classes [
                    "filled-icon"
                    CssIcons.``isax-setting-25``
                ]
            ]
        ]
    ]