module Components.DeviceController

open App
open Assets
open Feliz
open Fable.Core
open API.Domoticz



[<ReactComponent>]
let View (props: {| Device: Device; OnButtonPressed: (Idx -> DeviceAction -> unit) |}) =
    JsInterop.importAll "./DeviceController.sass"

    let i18n = I18n.pages.devices.buttons


    Html.div [
        prop.className "device-controller"
        prop.children [
            Html.p [
                prop.className "device-control-name"
                prop.text (
                    match props.Device.Name with
                    | name when name.Length > 20 -> name.[..17] + "..."
                    | name -> name
                )
            ]

            Html.div [
                prop.classes [
                    "device-control-buttons"
                    match props.Device.SubType with
                    | DeviceType.Blind -> "device-blind"
                    | DeviceType.ExtendedBlind -> "device-extended-blind"
                    | DeviceType.Switch -> "device-switch"
                ]
                prop.children [
                    match props.Device.SubType with
                    | DeviceType.Blind ->
                        Html.p [
                            prop.className "device-control-button"
                            prop.text i18n.on
                            prop.onClick (fun _event ->
                                DeviceAction.On
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                        Html.p [
                            prop.className "device-control-button"
                            prop.text i18n.off
                            prop.onClick (fun _event ->
                                DeviceAction.Off
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                    | DeviceType.ExtendedBlind ->
                        Html.span [
                            prop.classes [
                                "device-control-button"
                                CssIcons.``isax-arrow-up-2``
                            ]
                            prop.onClick (fun _event ->
                                DeviceAction.Off
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                        Html.span [
                            prop.classes [
                                "device-control-button"
                                CssIcons.``isax-stop``
                            ]
                            prop.onClick (fun _event ->
                                DeviceAction.Stop
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                        Html.span [
                            prop.classes [
                                "device-control-button"
                                CssIcons.``isax-arrow-down-14``
                            ]
                            prop.onClick (fun _event ->
                                DeviceAction.On
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                    | DeviceType.Switch ->
                        Html.p [
                            prop.className "device-control-button"
                            prop.text i18n.on
                            prop.onClick (fun _event ->
                                DeviceAction.Toggle
                                |> props.OnButtonPressed props.Device.idx
                            )
                        ]
                ]
            ]
        ]
    ]