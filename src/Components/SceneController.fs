module Components.SceneController

open App
open Assets
open Feliz
open Fable.Core.JsInterop
open API.Domoticz

importAll "./SceneController.sass"

[<ReactComponent>]
let View (props:
    {| SceneName: string
       SceneId: Idx
       OnActivate: Idx -> unit |}) =
    Html.div [
        prop.className "scene-controller"
        prop.children [
            Html.p [
                prop.className "scene-controller-name"
                prop.text props.SceneName
            ]

            Html.button [
                prop.className "scene-controller-button"
                prop.onClick (fun _ ->
                    props.SceneId |> props.OnActivate
                )
                prop.onContextMenu (fun _ ->
                    if Browser.isMobile then
                        props.SceneId |> props.OnActivate
                )
                prop.text I18n.pages.scenes.buttons.on
            ]
        ]
    ]