module Components.MaterialDesign.Chip

open Assets
open Feliz
open Fable.Core.JsInterop

importAll "./Chip.sass"

[<RequireQualifiedAccess>]
type Type =
    | Info
    | Error

[<ReactComponent>]
let View (props: {| UseIcon: bool; Text: string; ChipType: Type |}) =
    Html.div [
        prop.className
            (match props.ChipType with
                | Type.Info -> "chip info"
                | Type.Error -> "chip error" )
        prop.children [
            if props.UseIcon then
                Html.span [
                    prop.className
                        (match props.ChipType with
                        | Type.Info -> CssIcons.``isax-info-circle``
                        | Type.Error -> CssIcons.``isax-information``)
                ]

            Html.p [
                prop.text props.Text
            ]
        ]
    ]