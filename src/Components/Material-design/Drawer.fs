module Components.MaterialDesign.Drawer

open Feliz
open Fable.Core

module Types =
    type Label =
        { Name: string
          Icon: string
          Selected: bool
          OnPress: (unit -> unit) }

    type LabelGroup =
        { Name: string
          Children: Label list }

    [<RequireQualifiedAccess>]
    type DrawerElement =
        | Label of Label
        | LabelGroup of LabelGroup

    type DrawerCategory =
        { Name: string
          Items: DrawerElement list }


[<ReactComponent>]
let RenderDrawerItem drawerItem =
    match drawerItem with
    | Types.DrawerElement.Label label ->
        Html.div [
            prop.classes [
                "drawer-label"
                if label.Selected then "selected"
            ]
            prop.onClick (ignore >> label.OnPress)
            prop.onContextMenu (fun _ ->
                if Browser.isMobile then
                    label.OnPress()
            )
            prop.children [
                Html.span [
                    prop.classes [
                        "drawer-label-icon"
                        label.Icon
                    ]
                ]
                Html.p [
                    prop.className "drawer-label-name"
                    prop.text label.Name
                ]
            ]
        ]
    | Types.DrawerElement.LabelGroup labelGroup ->
        Html.div [
            prop.className "drawer-label-group"
            prop.text labelGroup.Name
            prop.children (
                labelGroup.Children
                |> List.map (fun label ->
                    Html.div [
                        prop.className "drawer-label"
                        prop.onClick (ignore >> label.OnPress)
                        prop.onContextMenu (fun _ ->
                            if Browser.isMobile then
                                label.OnPress()
                        )
                    ]
                )
            )
        ]

[<ReactComponent>]
let RenderCategory (props: {| drawerCategory: Types.DrawerCategory |}) =
    Html.div [
        prop.className "drawer-category"
        prop.children [
            Html.p [ prop.className "drawer-category-name"; prop.text props.drawerCategory.Name ]

            yield!
                props.drawerCategory.Items
                |> List.map RenderDrawerItem
        ]
    ]

JsInterop.importAll "./Drawer.sass"

[<ReactComponent>]
let View (props: {| IsOpened: bool; OnClose: (unit -> unit); Elements: Types.DrawerCategory list |}) =
    Html.div [
        prop.classes [
            "drawer-container"
            if props.IsOpened
            then "opened"
            else "closed"
        ]

        prop.onClick (ignore >> props.OnClose)
        prop.onContextMenu (fun _ ->
            if Browser.isMobile then
                props.OnClose()
        )

        prop.children [
            Html.div [
                prop.className "drawer"
                prop.children (
                    props.Elements
                    |> List.map (fun x -> {| drawerCategory = x |} |> RenderCategory)
                    |> List.toArray
                )
                prop.onClick (fun e -> e.stopPropagation())
            ]
        ]
    ]