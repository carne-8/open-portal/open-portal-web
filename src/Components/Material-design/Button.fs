module Components.MaterialDesign.Button

open Feliz
open Fable.Core.JsInterop

importAll "./Button.sass"

module CommonButton =
    [<ReactComponent>]
    let Filled (props: {| Text: string; OnPress: unit -> unit; IsSubmit: bool |}) =
        Html.button [
            if props.IsSubmit then
                prop.type' "submit"
            prop.className "filled-button"
            prop.onClick (ignore >> props.OnPress)
            prop.text props.Text
        ]

module FAB =
    [<ReactComponent>]
    let View (props: {| Name: string option; Icon: string; OnPress: unit -> unit; IsSubmit: bool |}) =
        Html.button [
            if props.IsSubmit then
                prop.type' "submit"
            prop.classes [
                "fab-button"
                if props.Name.IsSome then "large-button"
            ]
            prop.onClick (fun _ -> props.OnPress())
            prop.children [
                Html.span [
                    prop.className props.Icon
                ]

                match props.Name with
                | None -> ()
                | Some name ->
                    Html.p [
                        prop.text name
                    ]
            ]
        ]