module Components.MaterialDesign.TextInput

open Assets
open Feliz
open Fable.Core.JsInterop

importAll "./TextInput.sass"

[<ReactComponent>]
let View
    (props:
        {| Value: string
           Label: string
           Required: bool
           InputType: string
           OnTextChange: string -> unit
           OnRemovePressed: unit -> unit |}) =

    let (inputFilled, setInputFilled) = React.useState(false)
    let inputRef = React.useInputRef()

    React.useEffectOnce (fun _ ->
        match inputRef.current with
        | None -> ()
        | Some inputElement ->
            if inputElement.value <> "" then
                setInputFilled true
    )

    Html.div [
        prop.classes [
            "text-input"
            if inputFilled then "focused"
        ]
        prop.onClick (fun _ ->
            match inputRef.current with
            | None -> ()
            | Some inputElement ->
                inputElement.focus()
        )
        prop.children [
            Html.p [
                prop.text props.Label
            ]

            Html.input [
                prop.ref inputRef
                prop.type' props.InputType
                prop.required props.Required
                prop.value props.Value
                prop.onTextChange (fun text ->
                    match text with
                    | "" -> setInputFilled false
                    | _ -> setInputFilled true

                    text |> props.OnTextChange
                )
            ]

            Html.a [
                prop.className CssIcons.``isax-close-circle``
                prop.onClick (fun _ ->
                    match inputRef.current with
                    | None -> ()
                    | Some inputElement ->
                        inputElement.focus()
                        setInputFilled false

                    props.OnRemovePressed()
                )
            ]
        ]
    ]