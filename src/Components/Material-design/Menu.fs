module Components.MaterialDesign.Menu

open Feliz
open Fable.Core.JsInterop
open Browser

type MenuElement =
    | Item of {| Icon: string option; Name: string; OnPress: unit -> unit |}
    | Divider

[<ReactComponent>]
let RenderItem item =
    match item with
    | Divider -> Html.span [ prop.className "menu-divider" ]
    | Item item ->
        Html.a [
            prop.className "menu-item"
            prop.onClick (ignore >> item.OnPress)
            prop.onContextMenu (fun _ ->
                if isMobile then
                    item.OnPress()
            )
            prop.children [
                match item.Icon with
                | Some icon -> Html.span [ prop.classes [ "menu-item-icon"; icon ] ]
                | None -> ()

                Html.p [
                    prop.className "menu-item-name"
                    prop.text item.Name
                ]
            ]
        ]

[<ReactComponent>]
let View
    (props:
        {| OriginPosition: float * float
           Content: MenuElement list
           OnClose: unit -> unit |})
    =

    importAll "./Menu.sass"

    let posOpt, setPos = React.useState None
    let currentElement = React.useElementRef ()

    React.useEffectOnce (fun _ ->
        let element = currentElement.current.Value.getBoundingClientRect()
        let pageWidth = document.body.clientWidth
        let pageHeight = document.body.clientHeight

        printfn "%A" element.width

        let posLeft, posTop = props.OriginPosition
        let posRight = posLeft + element.width
        let posBottom = posTop + element.height

        let deltaX =
            if posLeft < 0. then
                0. - posLeft
            else if posRight > pageWidth then
                pageWidth - posRight - 10.
            else
                0.

        let deltaY =
            if posTop < 0. then
                0. - posTop
            else if posBottom > pageHeight then
                pageHeight - posBottom - 10.
            else
                0.

        let newX = posLeft + deltaX
        let newY = posTop + deltaY

        (newX, newY)
        |> Some
        |> setPos
    )


    Html.div [
        prop.className "menu-host"
        prop.onClick (fun e ->
            e.stopPropagation()
            props.OnClose()
        )
        prop.children [
            Html.div [
                prop.ref currentElement
                prop.className "menu"
                prop.onClick (fun e -> e.stopPropagation())
                match posOpt with
                | None -> ()
                | Some (left, top) ->
                    prop.style [
                        style.left (left |> int)
                        style.top (top |> int)
                    ]
                    prop.classes [ "menu"; "ready" ]
                prop.children (props.Content |> List.map RenderItem)
            ]
        ]
    ]