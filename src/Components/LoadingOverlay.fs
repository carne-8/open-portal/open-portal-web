module Components.LoadingOverlay

open App
open Feliz
open Fable.Core
open Fable.Core.JsInterop

open Lottie

importAll "./LoadingOverlay.sass"

[<ImportDefault("../assets/lotties/light-loading")>]
let lightAnimationData: obj = jsNative

[<ImportDefault("../assets/lotties/dark-loading")>]
let darkAnimationData: obj = jsNative

[<Emit("window.matchMedia('(prefers-color-scheme: dark)').matches")>]
let inline getDarkMode () : bool = jsNative

let View blurBackground =
    let animationData =
        match getDarkMode() with
        | false -> lightAnimationData
        | true -> darkAnimationData

    let lottieOptions: Lottie.Options =
        { loop = true
          autoplay = true
          animationData = animationData
          renderSettings =
            {| preserveAspectRatio = "xMidYMid slice" |} }

    Html.div [
        prop.classes [
            "loading"
            if blurBackground then "blur-background"
        ]
        prop.children [
            Html.div [
                prop.className "animation"
                prop.children [
                    Lottie.create [
                        Lottie.options lottieOptions
                        Lottie.isClickToPauseDisabled true
                    ]

                    Html.p [
                        prop.text I18n.app.loading
                    ]
                ]
            ]
        ]
    ]