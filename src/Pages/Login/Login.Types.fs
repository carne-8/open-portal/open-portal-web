namespace Pages.Login

open API.Domoticz.Types

type Form =
    { ServerAddress: string
      ServerPort: string
      Username: string
      Password: string }

[<RequireQualifiedAccess>]
type FormInput =
    | ServerAddress
    | ServerPort
    | Username
    | Password

[<RequireQualifiedAccess>]
type FormOperation =
    | TextChanged of string
    | Clear

[<RequireQualifiedAccess>]
type Msg =
    | FormChanged of FormInput * FormOperation
    | ValidateForm
    | RequestSucceed of Device []
    | RequestError of Thoth.Fetch.FetchError

[<RequireQualifiedAccess>]
type OutMsg =
    | FormValidated of ServerInfos * Device []

type Model =
    { Form: Form
      Loading: bool
      Error: string option }