module Pages.Login.View

open App
open Feliz
open Fable.Core
open Components.MaterialDesign

let View model dispatch =
    JsInterop.importAll "./Login.sass"

    let i18n = I18n.pages.login

    Html.div [
        prop.classes [ "page"; "login-page" ]
        prop.children [
            Html.form [
                prop.onSubmit (fun e ->
                    e.preventDefault()
                    Msg.ValidateForm |> dispatch
                )
                prop.children [
                    Html.h1 [
                        prop.text i18n.form.title
                    ]

                    TextInput.View
                        {| Value = model.Form.ServerAddress
                           Label = i18n.form.serverAddress
                           Required = true
                           InputType = "url"
                           OnTextChange = (fun newText -> (FormInput.ServerAddress, FormOperation.TextChanged newText) |> Msg.FormChanged |> dispatch)
                           OnRemovePressed = (fun _ -> (FormInput.ServerAddress, FormOperation.Clear) |> Msg.FormChanged |> dispatch) |}

                    TextInput.View
                        {| Value = model.Form.ServerPort |> string
                           Label = i18n.form.port
                           Required = true
                           InputType = "number"
                           OnTextChange = (fun newText -> (FormInput.ServerPort, FormOperation.TextChanged newText) |> Msg.FormChanged |> dispatch)
                           OnRemovePressed = (fun _ -> (FormInput.ServerPort, FormOperation.Clear) |> Msg.FormChanged |> dispatch) |}

                    TextInput.View
                        {| Value = model.Form.Username
                           Label = i18n.form.username
                           Required = true
                           InputType = "text"
                           OnTextChange = (fun newText -> (FormInput.Username, FormOperation.TextChanged newText) |> Msg.FormChanged |> dispatch)
                           OnRemovePressed = (fun _ -> (FormInput.Username, FormOperation.Clear) |> Msg.FormChanged |> dispatch) |}

                    TextInput.View
                        {| Value = model.Form.Password
                           Label = i18n.form.password
                           Required = true
                           InputType = "password"
                           OnTextChange = (fun newText -> (FormInput.Password, FormOperation.TextChanged newText) |> Msg.FormChanged |> dispatch)
                           OnRemovePressed = (fun _ -> (FormInput.Password, FormOperation.Clear) |> Msg.FormChanged |> dispatch) |}


                    Html.div [
                        prop.className "actions"
                        prop.children [
                            Button.CommonButton.Filled
                                {| Text = i18n.form.validate
                                   IsSubmit = true
                                   OnPress = ignore |}

                            match model.Error with
                            | Some error ->
                                Chip.View
                                    {| UseIcon = true
                                       Text = error
                                       ChipType = Chip.Type.Error |}
                            | _ -> ()
                        ]
                    ]
                ]
            ]

            if model.Loading then
                Components.LoadingOverlay.View true
        ]
    ]