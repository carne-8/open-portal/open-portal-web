namespace Pages.Scenes

open Elmish
open API.Domoticz

module Cmds =
    let launchScene serverInfos idx =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result =
                    Scene.launchScene serverInfos idx

                match result with
                | Error error ->
                    error
                    |> Thoth.Fetch.errorToString
                    |> Msg.RequestFailed
                    |> dispatch
                | Ok { message = (Some message); status = (Status.Error) } ->
                    message
                    |> Msg.RequestFailed
                    |> dispatch
                | Ok _response ->
                    Msg.RequestSucceed |> dispatch
            } |> Promise.start
        )

module State =
    let init scenes =
        { Scenes = scenes
          ErrorMessage = None }, Cmd.none

    let update serverInfos msg model =
        match msg with
        | Msg.SceneActivated idx ->
            model, Cmds.launchScene serverInfos idx

        | Msg.RequestSucceed ->
            model, Cmd.none

        | Msg.RequestFailed message ->
            { model with ErrorMessage = Some message },
            Cmd.none