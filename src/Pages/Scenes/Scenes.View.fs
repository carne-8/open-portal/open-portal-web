module Pages.Scenes.View

open App
open Feliz
open Fable.Core
open Components

open API.Domoticz

let View model dispatch =
    JsInterop.importAll "./Scenes.sass"

    let evenScenes =
        model.Scenes
        |> Array.allEven
        |> Array.map (fun scene ->
                SceneController.View
                    {| SceneName = scene.Name
                       SceneId = scene.idx
                       OnActivate = (Msg.SceneActivated >> dispatch) |}
            )

    let oddScenes =
        model.Scenes
        |> Array.allOdd
        |> Array.map (fun scene ->
                SceneController.View
                    {| SceneName = scene.Name
                       SceneId = scene.idx
                       OnActivate = (Msg.SceneActivated >> dispatch) |}
            )

    Html.div [
        prop.classes [ "page"; "device-page" ]
        prop.children [
            Html.p [
                prop.className "title"
                prop.text I18n.pages.scenes.title
            ]

            Html.div [
                prop.className "scenes"
                prop.children [
                    Html.div [
                        prop.className "scenes-column"
                        prop.children evenScenes
                    ]

                    Html.div [
                        prop.className "scenes-column"
                        prop.children oddScenes
                    ]
                ]
            ]

            match model.ErrorMessage with
            | None -> ()
            | Some errorMessage ->
                Html.div [
                    prop.className "error-chip"
                    prop.children (
                        MaterialDesign.Chip.View
                            {| ChipType = MaterialDesign.Chip.Type.Error
                               UseIcon = true
                               Text = errorMessage |}
                    )
                ]
        ]
    ]