namespace Pages.Scenes

open API.Domoticz

[<RequireQualifiedAccess>]
type Msg =
    | SceneActivated of Idx

    | RequestSucceed
    | RequestFailed of string

type Model =
    { Scenes: Scene []
      ErrorMessage: string option }