module Pages.DeviceControl.View

open App
open Feliz
open Fable.Core
open Components.MaterialDesign


let View model dispatch =
    JsInterop.importAll "./DeviceControl.sass"

    let i18n = I18n.pages.devices

    Html.div [
        prop.classes [ "page"; "device-page" ]
        prop.children [
            Html.p [
                prop.className "title"
                prop.text i18n.title
            ]

            Html.div [
                prop.className "devices"
                prop.children (
                    model.Devices
                    |> Array.map (fun device ->
                        Components.DeviceController.View
                            {| Device = device
                               OnButtonPressed = (fun idx deviceAction -> Msg.ButtonPressed (idx, deviceAction) |> dispatch) |}
                    )
                )
            ]

            match model.ErrorMessage with
            | None -> ()
            | Some errorMessage ->
                Html.div [
                    prop.className "error-chip"
                    prop.children (
                        Chip.View
                            {| ChipType = Chip.Type.Error
                               UseIcon = true
                               Text = errorMessage |}
                    )
                ]
        ]
    ]