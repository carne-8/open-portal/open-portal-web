namespace Pages.DeviceControl

open API.Domoticz

[<RequireQualifiedAccess>]
type Msg =
    | ButtonPressed of Idx * DeviceAction
    | SuccessRequest
    | ErrorOccurred of string

type Model =
    { Devices: API.Domoticz.Types.Device []
      ErrorMessage: string option }