namespace Pages.DeviceControl

open Elmish
open API.Domoticz

module Cmds =
    let executeDomoticzAction serverInfos deviceIdx deviceAction =
        Cmd.ofSub (fun dispatch ->
            promise {
                let! result =
                    Device.toggleDevice
                        serverInfos
                        deviceIdx
                        deviceAction

                match result with
                | Error error ->
                    error
                    |> Thoth.Fetch.errorToString
                    |> Msg.ErrorOccurred
                    |> dispatch
                | Ok { message = (Some message); status = (Status.Error) } ->
                    message
                    |> Msg.ErrorOccurred
                    |> dispatch
                | _ -> Msg.SuccessRequest |> dispatch

            } |> Promise.start
        )

module State =
    let init devices =
        { Devices = devices
          ErrorMessage = None }, Cmd.none

    let update serverInfos msg model : Model * Cmd<Msg> =
        match msg with
        | Msg.ButtonPressed (deviceIdx, button) ->
            model, Cmds.executeDomoticzAction serverInfos deviceIdx button

        | Msg.SuccessRequest ->
            { model with ErrorMessage = None }, Cmd.none

        | Msg.ErrorOccurred errorMessage ->
            { model with ErrorMessage = Some errorMessage }, Cmd.none