module Assets

open Zanaptak.TypedCssClasses

[<Literal>]
let icons = __SOURCE_DIRECTORY__ + "/assets/icon-font/style.css"
type CssIcons = CssClasses<icons, Naming.Verbatim>