const path = require("path")
const Dotenv = require("dotenv-webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CopyPlugin = require("copy-webpack-plugin")

const isProduction = !process.argv.find(v => v.indexOf("webpack-dev-server") !== -1)

module.exports = {
  mode: isProduction ? "production" : "development",
  devtool: isProduction ? "source-map" : "eval-source-map",
  entry: "./src/Main.fs.js",
  output: {
    path: path.resolve(__dirname, "build/"),
    filename: isProduction ? "[name].[contenthash].js" : "[name].js",
    publicPath: "auto"
  },
  devServer: {
    port: 8080,
    proxy: undefined,
    host: "0.0.0.0",
    hot: true,
    historyApiFallback: {
      rewrites: [
        { from: /./, to: "/index.html" }
      ]
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    }
  },
  plugins: [
    new Dotenv({
      systemvars: true
    }),
    new HtmlWebpackPlugin({
      template: "./src/index.html",
    }),
    new CopyPlugin({ patterns: [{ from: "./public" }] })
  ],
  module: {
    rules: [
      {
        test: /\.(sass|scss|css)$/,
        use: [
          "style-loader",
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              implementation: require("sass")
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?.*)?$/,
        type: "asset/resource"
      }
    ],
  }
}